# Marvel App

## Overview

The App currently has functionality for displaying an unfiltered list of comics and a filtered list of comics. Paging is also enabled to minimise upfront network requests.  I have not implemented functionality to tap through and see details as focused time on architecting the app in a way which would make it easy to build upon.

The App is constructed of 5 targets. The main “MarvelApp” target itself, and then 5 frameworks:
1. Auth - for enabling authenticated requests to the backend. I.e. providing valid keys, ts and hash values
2. Images - for fetching images
3. Plexus - for networking behaviour
4. ComicsList - for providing the screens and functionality for the list of comics and searching etc

All of these frameworks are individually compilable and testable via the various schemes. This means working on them in the future would be quick process and their public functionality can be easily shared among other modules by importing them.

### Approach

The ComicListViewController is built using an MVC approach. MVC seemed an appropriate architecture as there is no complex model to view transformation that would warrant using an MVVM approach.

With that in mind, an “interactor” is used to extract complex fetching / paging logic out of the viewcontroller. The viewcontroller interacts with the interactor through its abstraction, meaning if you wanted to unit test the viewcontroller you could by providing it with a mock interactor. For this reason, the interactor property is internal and not private.

As a first approach, paging is implemented utilising scroll view delegate methods, but with more time I would look to optimise this as the implementation currently has some limitations.

From a usability perspective, a loading spinner is used to show when a request is being made and there is some basic error handing. HTTP 500 errors will return a retry alert whereas other errors will display an alert but with no retry action. This is a fairly crude approach but given my limited knowledge of the API seemed sufficient as a demonstration of error handling. There is also a cell which is shown when there are no results for the search request.


### More details on frameworks

Plexus - 
Plexus provides ta mechanism for networking within the application. I have used the concept of a Resource to encapsulate networking behaviour. Essentially, an object which contains a URLRequest to make, and a transformation function taking in the returned data and url response, running validation on this and either throwing an error or successfully converting the data into your model objects.

Note how in the instance of decoding the comics, I have split the transformation into 2 steps. Firstly, “API” models are created from the raw JSON. These models all have optional properties on them for title, description and thumbnail. However, I then run a second conversion from these “API” models into models which will be used at the UI. This is where the main validation is done, only creating app models if there is a valid description and title. The reason for this approach is it increases the chance of the JSON being decoded successfully and not throwing errors due to maybe 1 out of 100 of the results being formatted incorrectly. This should enable a better user experience as you show whatever you are able. For more complex model structure it would also allow you to do more in depth conversions more easily than doing it as part of a decode. 

The reason for passing the URLResponse into the conversion function is because it allows you to generate errors specific to the API. In my experience, different APIs implement error codes slightly differently, so having the ability to interpret errors differently based upon the request you are making seems an advantage.

The conversion pathways from JSON to app models are tested in the ComicLists tests.

The design of Resource and plexus means adding functionality for a new API request is simple. You simply create another resource with a different request and transform but then call into the same generic pathways.

Images - 
I have created a separate struct ImageFetcher for fetching images. The primary reason for this was that it would enable more testability as the ImageFetcher has a static URLSession property on it which could be changed in a testing scenario. Although currently nothing else is being done, having the image fetching as a separate framework may be useful if you want to do any generic image optimisation / caching of images.


### What would I do next? (with more time)
The main outstanding work to do for this would be to create some form of MockURLSession which you can provide with specific data and a url response to return when a request is made to it. This would then make it possible to unit test the ImageFetcher, the Plexus networking pathway and the DefaultComicListInteractor. However, due to time I have not been able to do this.

The comics list page itself would be a good candidate for a UITest to test core functionality.

Image caching is not currently implemented within the app. This could be done using an NSCache, but personally I think a better approach would be for the API to have HTTP caching headers and to let the system handle the caching on its own. This would give the benefit of being able to change caching behaviour remotely rather than needing a code release to modify caching behaviour if required.
