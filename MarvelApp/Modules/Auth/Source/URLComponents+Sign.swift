//
//  Auth.swift
//  MarvelApp
//
//  Created by Dan King on 23/02/2020.
//  Copyright © 2020 Dan King. All rights reserved.
//

import Foundation

fileprivate enum AuthenticationKeys {
    static let ts = "time2"
    static let apiKey = "8c1d9ba6719a48fd12a152cf87d8e48b"
    static let hash = "3df1f5149a180d1a4b83b63c04eeaf5d"
}

extension URLComponents {
    
    /// Adds query paramters required for authentication by the API
    ///
    public func signed() -> Self {
        
        var components = URLComponents()
        components.scheme = self.scheme
        components.host = self.host
        components.path = self.path
        components.queryItems = [URLQueryItem(name: "ts", value: AuthenticationKeys.ts),
                                 URLQueryItem(name: "apikey", value: AuthenticationKeys.apiKey),
                                 URLQueryItem(name: "hash", value: AuthenticationKeys.hash)]
        
        if let currentQueryItems = self.queryItems {
            components.queryItems?.append(contentsOf: currentQueryItems)
        }
        
        return components
    }
}
