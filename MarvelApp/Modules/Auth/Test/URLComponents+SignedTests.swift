//
//  URLRequest+SignedTests.swift
//  AuthTests
//
//  Created by Dan King on 24/02/2020.
//  Copyright © 2020 Dan King. All rights reserved.
//

import XCTest
import Auth

class URLComponents_SignedTests: XCTestCase {

    func test_signed_withQueryItem_addsTs_addsAPIKey_addsHash() {
        
        var components = URLComponents()
        components.scheme = "https"
        components.host = "gateway.marvel.com"
        components.path = "/v1/public/comics"
        components.queryItems = [
            URLQueryItem(name: "query1", value: "something")
        ]
        
        let signed = components.signed()
        
        let expectedQueryItems = [URLQueryItem(name: "ts", value: "time2"),
                                  URLQueryItem(name: "apikey", value: "8c1d9ba6719a48fd12a152cf87d8e48b"),
                                  URLQueryItem(name: "hash", value: "3df1f5149a180d1a4b83b63c04eeaf5d"),
                                  URLQueryItem(name: "query1", value: "something")]
        
        XCTAssertEqual(signed.queryItems, expectedQueryItems)
    }
    
    func test_signed_noQueryItems_addsTs_addsAPIKey_addsHash() {
        
        var components = URLComponents()
        components.scheme = "https"
        components.host = "gateway.marvel.com"
        components.path = "/v1/public/comics"
        
        let signed = components.signed()
        
        let expectedQueryItems = [URLQueryItem(name: "ts", value: "time2"),
                                  URLQueryItem(name: "apikey", value: "8c1d9ba6719a48fd12a152cf87d8e48b"),
                                  URLQueryItem(name: "hash", value: "3df1f5149a180d1a4b83b63c04eeaf5d")]
        
        XCTAssertEqual(signed.queryItems, expectedQueryItems)
    }
}
