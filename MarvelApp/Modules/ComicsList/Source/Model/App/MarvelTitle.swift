//
//  AppModels.swift
//  MarvelApp
//
//  Created by Dan King on 22/02/2020.
//  Copyright © 2020 Dan King. All rights reserved.
//

import Foundation

struct MarvelComic {
    let title: String
    let description: String
    let thumbnailURL: URLRequest?
}

extension MarvelComic: Equatable { }
