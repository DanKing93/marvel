//
//  Thumbnail+ThumbnailURL.swift
//  MarvelApp
//
//  Created by Dan King on 23/02/2020.
//  Copyright © 2020 Dan King. All rights reserved.
//

import Foundation

extension API.ComicsResponse.MarvelData.MarvelComic.Thumbnail {
    
    func thumbnailURL() -> URLRequest? {
        
        guard let path = self.path,
            let `extension` = self.extension else { return nil }
        
        guard path != "",
            `extension` != "" else { return nil }
        
        let urlString = path + "/portrait_medium." + `extension`
        guard let url = URL(string: urlString) else { return nil }
        
        return URLRequest(url: url)
    }
}
