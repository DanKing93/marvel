//
//  Conversion.swift
//  MarvelApp
//
//  Created by Dan King on 22/02/2020.
//  Copyright © 2020 Dan King. All rights reserved.
//

import Foundation

extension MarvelComic {
    
    init?(_ marvelComic: API.ComicsResponse.MarvelData.MarvelComic) {
        
        guard let title = marvelComic.title,
            let description = marvelComic.description else { return nil }
        
        let thumbnailURL = marvelComic.thumbnail?.thumbnailURL()
        
        self.init(title: title, description: description, thumbnailURL: thumbnailURL)
    }
}

