//
//  ComicsResponseError.swift
//  MarvelApp
//
//  Created by Dan King on 23/02/2020.
//  Copyright © 2020 Dan King. All rights reserved.
//

import Foundation

extension API {
    
    enum ComicsResponseError: Error {
        case unauthorized401
        case notFound404
        case serverError500
    }
}

extension API.ComicsResponseError {
    
    init?(_ urlResponse: URLResponse) {
        
        guard let httpResponse = urlResponse as? HTTPURLResponse else { return nil }
        
        if httpResponse.statusCode == 401 { self = .unauthorized401
        } else if httpResponse.statusCode == 404 { self = .notFound404
        } else if httpResponse.statusCode == 500 { self = .serverError500
        } else { return nil }
    }
}
