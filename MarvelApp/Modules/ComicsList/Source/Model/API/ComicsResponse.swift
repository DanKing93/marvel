//
//  Models.swift
//  MarvelApp
//
//  Created by Dan King on 21/02/2020.
//  Copyright © 2020 Dan King. All rights reserved.
//

import Foundation

enum API { }

extension API {
    
    struct ComicsResponse: Codable {
        let data: MarvelData?
    }
}

extension API.ComicsResponse {
    
    struct MarvelData: Codable {
        let results: [MarvelComic]?
    }
}

extension API.ComicsResponse.MarvelData {
    
    struct MarvelComic: Codable {
        let title: String?
        let description: String?
        let thumbnail: Thumbnail?
    }
}

extension API.ComicsResponse.MarvelData.MarvelComic {
    
    struct Thumbnail: Codable {
        let path: String?
        let `extension`: String?
    }
}
