//
//  ImageLoadError.swift
//  MarvelApp
//
//  Created by Dan King on 23/02/2020.
//  Copyright © 2020 Dan King. All rights reserved.
//

import Foundation

struct ImageLoadError: Error { }
