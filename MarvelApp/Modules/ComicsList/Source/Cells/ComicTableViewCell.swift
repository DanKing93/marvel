//
//  ComicTableViewCell.swift
//  MarvelApp
//
//  Created by Dan King on 23/02/2020.
//  Copyright © 2020 Dan King. All rights reserved.
//

import UIKit
import Images

class ComicTableViewCell: UITableViewCell {
    
    @IBOutlet weak private var titleLabel: UILabel!
    @IBOutlet weak private var descriptionLabel: UILabel!
    @IBOutlet weak private var comicImageView: UIImageView!
    
    private var task: URLSessionDataTask? {
        willSet {
            task?.cancel()
        }
    }

    override func awakeFromNib() {
        super.awakeFromNib()
    }
    
    func setTitle(to title: String) {
        titleLabel.text = title
    }
    
    func setDescription(to description: String) {
        descriptionLabel.text = description
    }
    
    func setImageRequest(urlRequest: URLRequest) {
        fetchImage(with: urlRequest)
    }
    
    override func prepareForReuse() {
        super.prepareForReuse()
        
        task = nil
        comicImageView?.image = UIImage(named: "MarvelLogoPDF")
    }
}

extension ComicTableViewCell {
    
    func fetchImage(with request: URLRequest) {
        
        task = ImageFetcher.fetch(resource: .image(for: request)) { result in
            
            if case .success(let image) = result {
                self.comicImageView.image = image
            }
        }
    }
}
