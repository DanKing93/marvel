//
//  ComicListInteractor.swift
//  MarvelApp
//
//  Created by Dan King on 23/02/2020.
//  Copyright © 2020 Dan King. All rights reserved.
//

import Foundation

protocol ComicListInteractor {
    func fetchUnfilteredList(_ completion: @escaping (Result<[MarvelComic], Error>) -> Void)
    func fetchFilteredList(searchTerm: String, _ completion: @escaping (Result<[MarvelComic], Error>) -> Void)
    func fetchNextPage(_ completion: @escaping (Result<[MarvelComic], Error>) -> Void)
    func retry(_ completion: @escaping (Result<[MarvelComic], Error>) -> Void)
}
