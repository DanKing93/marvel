//
//  Errors.swift
//  ComicsList
//
//  Created by Dan King on 25/02/2020.
//  Copyright © 2020 Dan King. All rights reserved.
//

import Foundation

struct RetryError: Error { }
struct SearchTermEncodingError: Error { }
