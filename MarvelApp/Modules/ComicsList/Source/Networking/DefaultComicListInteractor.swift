//
//  ComicListInteractor.swift
//  MarvelApp
//
//  Created by Dan King on 23/02/2020.
//  Copyright © 2020 Dan King. All rights reserved.
//

import Foundation
import Plexus

class DefaultComicListInteractor: ComicListInteractor {
    
    private var urlSession: URLSession
    private var task: URLSessionDataTask?
    private var lastFetchedPagedResource = PagedResource(offset: 0, searchTerm: nil)
    private var isFetching = false
    
    init(urlSession: URLSession = URLSession.shared) {
        self.urlSession = urlSession
    }
    
    func fetchUnfilteredList(_ completion: @escaping (Result<[MarvelComic], Error>) -> Void) {
        
        guard !isFetching else { return }
        
        lastFetchedPagedResource = PagedResource(offset: 0, searchTerm: nil)
        isFetching = true
        
        task = urlSession.fetch(lastFetchedPagedResource.resource()) { [weak self] result in
            
            self?.isFetching = false
            
            DispatchQueue.main.async {
               completion(result)
            }
        }
    }
    
    func fetchFilteredList(searchTerm: String, _ completion: @escaping (Result<[MarvelComic], Error>) -> Void) {
        
        guard !isFetching else { return }
        
        guard let encodedString = searchTerm.lowercased().addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed) else {
            completion(.failure(SearchTermEncodingError()))
            return
        }
        
        lastFetchedPagedResource = PagedResource(offset: 0, searchTerm: encodedString)
        isFetching = true
        
        task = urlSession.fetch(lastFetchedPagedResource.resource()) { [weak self] result in
            
            self?.isFetching = false
            DispatchQueue.main.async {
               completion(result)
            }
        }
    }
    
    func fetchNextPage(_ completion: @escaping (Result<[MarvelComic], Error>) -> Void) {
        
        guard !isFetching else { return }
        
        lastFetchedPagedResource = lastFetchedPagedResource.nextPage()
        isFetching = true
        
        task = urlSession.fetch(lastFetchedPagedResource.resource()) { [weak self] result in
            
            guard let self = self else { return }
            self.isFetching = false
            
            // make sure we don't keep adding offset if previous one failed
            if case .failure(_) = result {
                self.lastFetchedPagedResource = self.lastFetchedPagedResource.previousPage()
            }
            
            DispatchQueue.main.async {
               completion(result)
            }
        }
    }
    
    func retry(_ completion: @escaping (Result<[MarvelComic], Error>) -> Void) {
        
        guard !isFetching else { return }
        isFetching = true
        
        task = urlSession.fetch(lastFetchedPagedResource.resource()) { [weak self] result in
            
            self?.isFetching = false
            
            DispatchQueue.main.async {
                completion(result)
            }
        }
    }
}

fileprivate struct PagedResource {
    let offset: Int
    let searchTerm: String?
}

extension PagedResource {
    
    func resource() -> Resource<[MarvelComic]> {
        return .comics(forSearchTerm: searchTerm, withOffset: offset)
    }
    
    func nextPage() -> Self {
        let newOffset = offset + 50
        return PagedResource(offset: newOffset,
                             searchTerm: searchTerm)
    }
    
    func previousPage() -> Self {
        let newOffset = offset - 50
        return PagedResource(offset: newOffset,
                             searchTerm: searchTerm)
    }
}
