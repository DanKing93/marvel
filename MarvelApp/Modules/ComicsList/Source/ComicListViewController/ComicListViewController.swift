//
//  ViewController.swift
//  MarvelApp
//
//  Created by Dan King on 21/02/2020.
//  Copyright © 2020 Dan King. All rights reserved.
//

import UIKit

class ComicListViewController: UIViewController {
    
    @IBOutlet weak private var tableView: UITableView!
    @IBOutlet weak private var loadingView: LoadingView!
    @IBOutlet weak private var searchBar: UISearchBar!
    
    // Internal so it could be mocked in tests
    var interactor: ComicListInteractor = DefaultComicListInteractor()
    
    private var task: URLSessionDataTask?
    private var comics = [MarvelComic]()

    override func viewDidLoad() {
        super.viewDidLoad()
        
        title = "Marvel Comics"
        navigationController?.navigationBar.prefersLargeTitles = true
        
        configureTableView()
        configureSearchBar()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        
        fetchUnfilteredComics()
    }
    
    private func configureTableView() {
        tableView.dataSource = self
        tableView.delegate = self
        tableView.tableFooterView = UIView()
        tableView.keyboardDismissMode = .onDrag
        
        (tableView as UIScrollView).delegate = self
    }
    
    private func configureSearchBar() {
        searchBar.delegate = self
        searchBar.placeholder = "Search title name"
    }
    
    private func showLoadingView() {
        view.isUserInteractionEnabled = false
        loadingView.startLoading()
    }
    
    private func hideLoadingView() {
        view.isUserInteractionEnabled = true
        loadingView.stopLoading()
    }
    
    private func handleError(_ error: Error) {
        
        guard let comicError = error as? API.ComicsResponseError else {
            presentFailureError()
            return
        }
        
        switch comicError {
        case .serverError500: presentRetryError()
        case .notFound404: presentFailureError()
        case .unauthorized401: presentFailureError()
        }
    }
    
    private func presentRetryError() {
        
        let alert = UIAlertController(title: "Something went wrong", message: "Please retry", preferredStyle: .alert)
        alert.addAction(UIAlertAction(title: "Retry", style: .default, handler: { _ in
            self.retryFetch()
        }))
        
        present(alert, animated: true, completion: nil)
    }
    
    private func presentFailureError() {
        
        let alert = UIAlertController(title: "Something went wrong", message: "Please come back later", preferredStyle: .alert)
        alert.addAction(UIAlertAction(title: "OK", style: .default, handler: nil))
        
        present(alert, animated: true, completion: nil)
    }
}

// MARK: - Table View Data Source and Delegate

extension ComicListViewController: UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        comics.isEmpty ? 1 : comics.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        guard !comics.isEmpty else {
            return tableView.dequeueReusableCell(withIdentifier: "NoResultsTableViewCell") ?? UITableViewCell()
        }
        
        guard let comicCell = tableView.dequeueReusableCell(withIdentifier: "ComicTableViewCell") as? ComicTableViewCell else {
            return UITableViewCell()
        }
        
        let comic = comics[indexPath.row]
        
        comicCell.setTitle(to: comic.title)
        comicCell.setDescription(to: comic.description)
        
        if let thumbnailRequest = comic.thumbnailURL {
            comicCell.setImageRequest(urlRequest: thumbnailRequest)
        }
        
        return comicCell
    }
}

extension ComicListViewController: UITableViewDelegate {
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
    }
}

// MARK: - Search Bar Delegate

extension ComicListViewController: UISearchBarDelegate {
    
    func searchBarSearchButtonClicked(_ searchBar: UISearchBar) {
            
        searchBar.searchTextField.resignFirstResponder()
        
        guard let searchTerm = searchBar.text,
            searchTerm != "" else {
                fetchUnfilteredComics()
                return
        }
        
        fetchFilteredComics(for: searchTerm)
    }
}

// MARK: - Scroll View Delegate

extension ComicListViewController: UIScrollViewDelegate {
    
    func scrollViewDidEndDragging(_ scrollView: UIScrollView, willDecelerate decelerate: Bool) {
        
        guard !comics.isEmpty else { return }
        guard let last = tableView.visibleCells.last else { return }

        if tableView.indexPath(for: last)?.row == (comics.count - 1) {
            fetchNextPage()
        }
    }
}

// MARK: - Network Requests

extension ComicListViewController {
    
    private func fetchUnfilteredComics() {
        
        showLoadingView()
        
        interactor.fetchUnfilteredList { [unowned self] result in
            
            self.hideLoadingView()
            
            switch result {
            case .success(let comics):
                self.comics = comics
                self.tableView.reloadData()
                self.tableView.scrollToRow(at: IndexPath(row: 0, section: 0), at: .top, animated: true)
                
            case .failure(let error):
                self.handleError(error)
            }
        }
    }
    
    private func fetchFilteredComics(for searchTerm: String) {
        
        showLoadingView()
        
        interactor.fetchFilteredList(searchTerm: searchTerm) { [unowned self] result in
            
            self.hideLoadingView()
            
            switch result {
            case .success(let comics):
                self.comics = comics
                self.tableView.reloadData()
                self.tableView.scrollToRow(at: IndexPath(row: 0, section: 0), at: .top, animated: true)
                
            case .failure(let error):
                self.handleError(error)
            }
        }
    }
    
    private func fetchNextPage() {
        
        interactor.fetchNextPage { [unowned self] result in
            
            if case .success(let newComics) = result {
                
                let finalIndex = (self.comics.count - 1)
                let numberToAdd = newComics.count
                
                guard numberToAdd >= 1 else { return }
                
                let indexes = (1...numberToAdd).map { IndexPath(row: finalIndex + $0, section: 0) }
                
                self.comics.append(contentsOf: newComics)
                self.tableView.insertRows(at: indexes, with: .automatic)
            }
        }
    }
    
    private func retryFetch() {
        
        showLoadingView()
        
        interactor.retry { [unowned self] result in
            
            self.hideLoadingView()
            
            switch result {
            case .success(let comics):
                self.comics = comics
                self.tableView.reloadData()
                
            case .failure(let error):
                self.handleError(error)
            }
        }
    }
}

