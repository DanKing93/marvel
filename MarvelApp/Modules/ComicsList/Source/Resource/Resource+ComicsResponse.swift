//
//  Resource.swift
//  MarvelApp
//
//  Created by Dan King on 21/02/2020.
//  Copyright © 2020 Dan King. All rights reserved.
//

import Foundation
import Auth
import Plexus

extension Resource where Value == [MarvelComic] {
    
    static func comics(forSearchTerm searchTerm: String?,
                       withOffset offset: Int) -> Resource<[MarvelComic]> {
        
        var components = URLComponents()
        
        components.scheme = "https"
        components.host = "gateway.marvel.com"
        components.path = "/v1/public/comics"
        components.queryItems = [
            URLQueryItem(name: "limit", value: "50"),
            URLQueryItem(name: "offset", value: "\(offset)")
        ]
        
        if let searchTerm = searchTerm {
            components.queryItems?.append(URLQueryItem(name: "titleStartsWith", value: searchTerm))
        }
        
        let request = URLRequest(url: components.signed().url!)
        
        return Resource(request: request) { data, response in
            
            if let error = API.ComicsResponseError(response) { throw error }
            
            let unfilteredResponse = try JSONDecoder().decode(API.ComicsResponse.self, from: data)
            let marvelTitles = unfilteredResponse.data?.results?.compactMap { MarvelComic($0) }
            return marvelTitles ?? []
        }
    }
}
