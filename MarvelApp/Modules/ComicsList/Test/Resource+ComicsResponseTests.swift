//
//  Resource+ComicsResponseTests.swift
//  MarvelAppTests
//
//  Created by Dan King on 23/02/2020.
//  Copyright © 2020 Dan King. All rights reserved.
//

import XCTest
@testable import ComicsList
import Plexus

class Resource_ComicsResponseTests: XCTestCase {

    // MARK: - Request Structure

    func test_resource_request_withoutSearchTerm() {

        let resource = Resource.comics(forSearchTerm: nil, withOffset: 0)

        XCTAssertEqual(resource.request.url?.absoluteString, "https://gateway.marvel.com/v1/public/comics?ts=time2&apikey=8c1d9ba6719a48fd12a152cf87d8e48b&hash=3df1f5149a180d1a4b83b63c04eeaf5d&limit=50&offset=0")
    }

    func test_resource_request_withSearchTerm_withOffset() {

        let resource = Resource.comics(forSearchTerm: "hulk", withOffset: 50)

        XCTAssertEqual(resource.request.url?.absoluteString, "https://gateway.marvel.com/v1/public/comics?ts=time2&apikey=8c1d9ba6719a48fd12a152cf87d8e48b&hash=3df1f5149a180d1a4b83b63c04eeaf5d&limit=50&offset=50&titleStartsWith=hulk")
    }

    // MARK: - Decode and Model Conversion

    func test_resource_decodeAndConversion() throws {

        let resource = Resource.comics(forSearchTerm: nil, withOffset: 0)

        let data = try loadJSON(withFilename: "TestComicResponse")
        let mockHTTPResponse = HTTPURLResponse(statusCode: 200)
        let expectedImageURLRequest = URLRequest(url: URL(string: "http://anImage.com/portrait_medium.jpg")!)

        let marvelTitles = try resource.transform((data, mockHTTPResponse))

        let expectedArray = [MarvelComic(title: "Title 1",
                                         description: "Description 1",
                                         thumbnailURL: expectedImageURLRequest),
                             MarvelComic(title: "Title 4",
                                         description: "Description 4",
                                         thumbnailURL: nil),
                             MarvelComic(title: "Title 5",
                                         description: "Description 5",
                                         thumbnailURL: nil)]

        XCTAssertEqual(marvelTitles, expectedArray)
    }

    // MARK: - HTTP Errors

    func test_http401_throwsUnauthorized401Error() throws {

        let resource = Resource.comics(forSearchTerm: nil, withOffset: 0)

        let data = try loadJSON(withFilename: "TestComicResponse")
        let mockHTTPResponse = HTTPURLResponse(statusCode: 401)

        var thrownError: Error?
        XCTAssertThrowsError(try resource.transform((data, mockHTTPResponse)), "Threw") { error in
            thrownError = error
        }

        XCTAssertTrue(thrownError is API.ComicsResponseError)
        XCTAssertEqual(thrownError as? API.ComicsResponseError, .unauthorized401)
    }

    func test_http404_throwsNotFound404Error() throws {

        let resource = Resource.comics(forSearchTerm: nil, withOffset: 0)

        let data = try loadJSON(withFilename: "TestComicResponse")
        let mockHTTPResponse = HTTPURLResponse(statusCode: 404)

        var thrownError: Error?
        XCTAssertThrowsError(try resource.transform((data, mockHTTPResponse)), "Threw") { error in
            thrownError = error
        }

        XCTAssertTrue(thrownError is API.ComicsResponseError)
        XCTAssertEqual(thrownError as? API.ComicsResponseError, .notFound404)
    }

    func test_http500_throwsServerError500Error() throws {

        let resource = Resource.comics(forSearchTerm: nil, withOffset: 0)

        let data = try loadJSON(withFilename: "TestComicResponse")
        let mockHTTPResponse = HTTPURLResponse(statusCode: 500)

        var thrownError: Error?
        XCTAssertThrowsError(try resource.transform((data, mockHTTPResponse)), "Threw") { error in
            thrownError = error
        }

        XCTAssertTrue(thrownError is API.ComicsResponseError)
        XCTAssertEqual(thrownError as? API.ComicsResponseError, .serverError500)
    }
}

extension Resource_ComicsResponseTests {

    // MARK: - Helpers

    private func loadJSON(withFilename filename: String) throws -> Data {

        let bundle = Bundle(for: Resource_ComicsResponseTests.self)

        guard let path = bundle.path(forResource: filename, ofType: "json"),
            let data = try? Data(contentsOf: URL(fileURLWithPath: path), options: .mappedIfSafe) else {
                XCTFail("Couldn't generate JSON Data")
                throw JSONError()
        }

        return data
    }
}

fileprivate struct JSONError: Error { }

fileprivate extension HTTPURLResponse {

    convenience init(statusCode: Int) {
        self.init(url: URL(string: "https://www.johnlewis.com")!,
                  statusCode: statusCode,
                  httpVersion: nil,
                  headerFields: nil)!
    }
}
