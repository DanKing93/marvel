//
//  Resource.swift
//  MarvelApp
//
//  Created by Dan King on 22/02/2020.
//  Copyright © 2020 Dan King. All rights reserved.
//

import Foundation

/// Resource for fetching data from network
public struct Resource<Value> {
    
    public typealias Response = (Data, URLResponse)
    
    /// URLRequest to fetch with
    public let request: URLRequest
    
    /// Function transforming data and URLResponse obtained from
    /// network request into a value
    public let transform: (Response) throws -> Value
    
    public init(request: URLRequest,
                transform: @escaping (Response) throws -> Value) {
        self.request = request
        self.transform = transform
    }
}
