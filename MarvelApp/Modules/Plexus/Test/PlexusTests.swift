//
//  PlexusTests.swift
//  PlexusTests
//
//  Created by Dan King on 24/02/2020.
//  Copyright © 2020 Dan King. All rights reserved.
//

import XCTest
@testable import Plexus

class PlexusTests: XCTestCase {

    func test_somethingToCompile() {
        let string = "String"
        XCTAssertEqual(string, "String")
    }
}
