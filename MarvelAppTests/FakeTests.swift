//
//  FakeTests.swift
//  MarvelAppTests
//
//  Created by Dan King on 24/02/2020.
//  Copyright © 2020 Dan King. All rights reserved.
//

import XCTest

class FakeTests: XCTestCase {

    override func setUp() {
        // Put setup code here. This method is called before the invocation of each test method in the class.
    }

    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
    }
    
    func test_somethingToCompile() {
        let string = "String"
        XCTAssertEqual(string, "String")
    }
}
